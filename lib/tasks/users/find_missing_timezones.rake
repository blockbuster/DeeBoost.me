namespace :users do
  desc "find timezones for locations that don't have one"
  task find_missing_timezones: :environment do
    user = User.find_by email: "dragos.calin.wrk+1@gmail.com"
    user.locations.where(timezone: "").each do |location|
      TimezoneWorker.perform_async location.id
    end
  end
end