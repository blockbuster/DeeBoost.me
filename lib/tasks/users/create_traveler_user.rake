namespace :users do
  desc 'create a user with locations from all over the world'
  task create_traveler_user: :environment do
    create_user
  end
end

def create_user
  user = User.create email: "dragos.calin.wrk+1@gmail.com",
              password: "random2kx",
              password_confirmation: "random2kx"
  locations.each do |location_hash|
    location = user.locations.create location_hash
    if location.id
      TimezoneWorker.perform_async location.id
    end
  end
end

def locations
  [
    # Bucharest
    {
     latitude: 44.43225,
     longitude: 26.10626,
     current_location: true
    },
    # New York
    {
     latitude: 40.712783,
     longitude: -74.005941,
     current_location: false
    },
    # London
    {
     latitude: 51.50853,
     longitude: -0.12574,
     current_location: false
    },
    # Paris
    {
     latitude: 48.8488459,
     longitude: 2.3356589,
     current_location: false
    },
    # Rome
    {
     latitude: 41.9102411,
     longitude: 12.3955717,
     current_location: false
    },
    # Hamburg
    {
     latitude: 53.5586937,
     longitude: 9.7873966,
     current_location: false
    },
    # Athens
    {
     latitude: 37.9908996,
     longitude: 23.7032341,
     current_location: false
    },
    # Istanbul
    {
     latitude: 41.0054989,
     longitude: 28.7313124,
     current_location: false
    },
    # Moscow
    {
     latitude: 55.7498582,
     longitude: 37.3516351,
     current_location: false
    },
    # Baghdad
    {
     latitude: 33.311894,
     longitude: 44.215478,
     current_location: false
    },
    # Tehrand
    {
     latitude: 35.6970456,
     longitude: 51.0689494,
     current_location: false
    },
    # Yekaterinburg
    {
     latitude: 56.8140001,
     longitude: 60.514509,
     current_location: false
    },
    # Islamabad
    {
     latitude: 33.6693409,
     longitude: 72.8445503,
     current_location: false
    },
    # New Delhi
    {
     latitude: 28.6457545,
     longitude: 76.8098858,
     current_location: false
    },
    # Omsk
    {
     latitude: 54.9859465,
     longitude: 73.0752787,
     current_location: false
    },
    # Irkutsk
    {
     latitude: 52.298514,
     longitude: 104.126733,
     current_location: false
    },
    # Vladivostok
    {
     latitude: 43.1738705,
     longitude: 131.895411,
     current_location: false
    },
    # Beijing
    {
     latitude: 39.9390715,
     longitude: 116.1165914,
     current_location: false
    },
    # Shanghai
    {
     latitude: 31.2243026,
     longitude: 120.914945,
     current_location: false
    },
    # Taipei
    {
     latitude: 25.0689536,
     longitude: 121.4730179,
     current_location: false
    },
    # Hong Kong
    {
     latitude: 22.359718,
     longitude: 113.8599954,
     current_location: false
    },
    # Seoul
    {
     latitude: 37.565289,
     longitude: 126.8491234,
     current_location: false
    },
    # Tokyo
    {
     latitude: 35.6693873,
     longitude: 139.6009539,
     current_location: false
    },
    # Perth
    {
     latitude: -31.9679919,
     longitude: 115.650474,
     current_location: false
    },
    # Alice Springs
    {
     latitude: -23.6995104,
     longitude: 133.8763227,
     current_location: false
    },
    # Adelaide
    {
     latitude: -34.9850792,
     longitude: 138.4210894,
     current_location: false
    },
    # Melbourne
    {
     latitude: -37.8578638,
     longitude: 144.5178359,
     current_location: false
    },
    # Sydney
    {
     latitude: -33.7960346,
     longitude: 150.6415664,
     current_location: false
    },
    # Brisbane
    {
     latitude: -27.4050449,
     longitude: 152.4410586,
     current_location: false
    },
    # Auckland
    {
     latitude: -36.8621432,
     longitude: 174.5846047,
     current_location: false
    },
    # Honolulu
    {
     latitude: 21.3282953,
     longitude: -157.9394105,
     current_location: false
    },
    # Los Angeles
    {
     latitude: 34.0207488,
     longitude: -118.6926023,
     current_location: false
    },
    # San Francisco
    {
     latitude: 37.7578149,
     longitude: -122.507811,
     current_location: false
    },
    # Portland
    {
     latitude: 45.5425909,
     longitude: -122.7948479,
     current_location: false
    },
    # Vancouver
    {
     latitude: 49.2562175,
     longitude: -123.1941246,
     current_location: false
    },
    # Anchorage
    {
     latitude: 61.1083398,
     longitude: -149.9348908,
     current_location: false
    },
    # Denver
    {
     latitude: 39.7645283,
     longitude: -105.135979,
     current_location: false
    },
    # Houston
    {
     latitude: 29.817476,
     longitude: -95.6821626,
     current_location: false
    },
    # Chicago
    {
     latitude: 41.8339032,
     longitude: -87.8725817,
     current_location: false
    },
    # Nashville
    {
     latitude: 36.1868667,
     longitude: -87.0661144,
     current_location: false
    },
    # Atlanta
    {
     latitude: 33.7679188,
     longitude: -84.5610309,
     current_location: false
    },
    # Miami
    {
     latitude: 25.7824617,
     longitude: -80.3012917,
     current_location: false
    },
    # Pittsburgh
    {
     latitude: 40.4314779,
     longitude: -80.0507118,
     current_location: false
    },
    # Washington
    {
     latitude: 38.8994613,
     longitude: -77.0847779,
     current_location: false
    },
    # New York
    {
     latitude: 40.70583,
     longitude: -74.2588707,
     current_location: false
    },
    # Boston
    {
     latitude: 42.3135417,
     longitude: -71.1975832,
     current_location: false
    },
    # Toronto
    {
     latitude: 43.7183937,
     longitude: -79.6589238,
     current_location: false
    },
    # Sao Paulo
    {
     latitude: -23.6815303,
     longitude: -46.8761694,
     current_location: false
    },
    # Buenos Aires
    {
     latitude: -34.6156624,
     longitude: -58.5035315,
     current_location: false
    },
    # Rio de Janeiro
    {
     latitude: -22.9103541,
     longitude: -43.7292044,
     current_location: false
    },
    # Cape Town
    {
     latitude: -33.9140975,
     longitude: 18.3751933,
     current_location: false
    },
    # Pretoria
    {
     latitude: -25.757762,
     longitude: 27.9170811,
     current_location: false
    },
    # Reykjavik
    {
     latitude: 64.1325242,
     longitude: -21.9928658,
     current_location: false
    },
    # Lisbon
    {
     latitude: 38.7437395,
     longitude: -9.2304151,
     current_location: false
    },
    # Glasgow
    {
     latitude: 55.855573,
     longitude: -4.3728832,
     current_location: false
    }
  ]
end

